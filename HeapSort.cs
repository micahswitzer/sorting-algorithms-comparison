// Author: Kurtis

namespace SortingAlgorithmsComparison
{
    public static class HeapSort
    {
        public static int[] Sort(int[] heap)
        {
            //create "heap"
            int size = heap.Length - 1;
            //starting at second-to-bottom level parent, checking its children
            int i = (size - 2) / 2;
            while (i >= 0)
            {
                Heapify(heap, size, i);
                --i;
            }

            // remove root and place at nth index of array. Take the last element of the heap and make it the root
            while (size >= 0)
            {
                Swap(heap, 0, size);
                //reduce size of heap by one, heapify & repeat
                Heapify(heap, size--, 0);
            }

            return heap;
        }

        private static void Heapify(int[] heap, int heapSize, int i)
        {
            int largeIndex = i;
            int lChildIndex = 2 * i + 1;
            int rChildIndex = 2 * i + 2;

            // if the child indices are within the bounds of the tree, get max
            if (lChildIndex < heapSize && heap[lChildIndex] > heap[largeIndex])
            {
                largeIndex = lChildIndex;
            }
            if (rChildIndex < heapSize && heap[rChildIndex] > heap[largeIndex])
            {
                largeIndex = rChildIndex;
            }

            // if largeIndex is not the original node we were looking at, swap and heapify at this next level of the tree
            if (largeIndex != i)
            {
                Swap(heap, i, largeIndex);
                Heapify(heap, heapSize, largeIndex);
            }
        }

        private static void Swap(int[] heap, int index1, int index2)
        {
            int temp = heap[index1];
            heap[index1] = heap[index2];
            heap[index2] = temp;
        }
    }
}
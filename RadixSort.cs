using System.Linq;

namespace SortingAlgorithmsComparison
{
    public static class RadixSort
    {
        public static int[] Sort(int[] arr)
        {
            // find the max value
            var max = arr.Max();

            // sort!
            var p = 1;
            while (max / p > 0)
            {
                arr = CountingSort(arr, p);
                p *= 10;
            }

            return arr;
        }

        private static int[] CountingSort(int[] arr, int p)
        {
            int[] result = new int[arr.Length];
            int[] count = new int[10];

            // counting pass
            for (int i = 0; i < arr.Length; ++i)
                ++count[(arr[i] / p) % 10];
            // compute the position of each digit
            for (int i = 1; i < 10; ++i)
                count[i] += count[i - 1];
            // count starting at the end, working back towards the begining
            for (int i = arr.Length - 1; i >= 0; --i)
                result[--count[(arr[i] / p) % 10]] = arr[i];
            
            return result;
        }  
    }
}
﻿using System;
using System.Linq;
using System.Reflection;
using BenchmarkDotNet.Running;

namespace SortingAlgorithmsComparison
{
    class Program
    {
        static void Main(string[] args)
        {
            // run the benchmark
            var summary = BenchmarkRunner.Run<Benchmark>();
        }
    }
}

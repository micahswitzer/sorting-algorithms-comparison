//Author: Kurtis

namespace SortingAlgorithmsComparison
{
    public static class InsertionSort
    {
        public static int[] Sort(int[] arr)
        {
            //grab the next element of interest
            for (int i = 1; i < arr.Length; ++i) {
                int curr = arr[i];
                
                int j = i - 1;
                //if the element(s) behind curr are greater
                while (j >= 0 && arr[j] > curr) {
                    //replace curr's element with element behind it
                    arr[j+1] = arr[j];
                    //decrement j to get correct previous element in next iteration
                    --j; 
                }
                //complete the swap by setting the current position to the curr value
                arr[j+1] = curr;
            }
            return arr;
        }
    }
}

using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Engines;

namespace SortingAlgorithmsComparison
{
    [CsvExporter]
    [SimpleJob(RunStrategy.ColdStart, targetCount: 5)]
    public class Benchmark
    {
        [Params(100, 500, 1_000, 5_000, 1_0000, 50_000,
            100_000, 500_000, 1_000_000, 5_000_000, 10_000_000)]
        public int N;

        private int[] arr;

        [IterationSetup]
        public void Setup()
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            arr = new int[N];
            for (int i = 0; i < N; ++i)
                arr[i] = rand.Next();
        }

        [Benchmark]
        public int[] Insertion() => InsertionSort.Sort(arr);
        
        [Benchmark]
        public int[] Radix() => RadixSort.Sort(arr);

        [Benchmark]
        public int[] Heap() => HeapSort.Sort(arr);
    }
}
namespace SortingAlgorithmsComparison
{
    public interface ISortingAlgorithm
    {
        int[] Sort(int[] data);
    }
}